//jeu à la Geometry Wars

var canvas = null;
var ctx = null;
var bob = null;
var score = 0;
var spriteList = new Array();
var bulletList = new Array();

var canvasWidth = 1024;
var canvasHeight = 768;

var upPressed = false;
var downPressed = false;
var leftPressed = false;
var rightPressed = false;
var mouseLeftPressed = false;

var mousePos = (canvasWidth/2, canvasHeight/2);

$(document).ready( function(){
	container = document.createElement('div');
	container.style.id = "container";	
	container.style.width = canvasWidth;
	container.style.margin = "50px 100px";
	
	canvas = document.createElement('canvas');
	canvas.id = "canvas";
	canvas.width = canvasWidth;
	canvas.height = canvasHeight;
	canvas.style.zIndex = 8;
	canvas.style.border = "2px solid white";
	canvas.style.tabindex = "1";
	canvas.style.cursor = "crosshair";
	
	container.appendChild(canvas);
	document.body.appendChild(container);
	
	canvas = document.getElementById("canvas");
	ctx = canvas.getContext("2d");
	
	canvas.onmousemove =  function(evt) {
        mousePos = getMousePos(evt);
	}
	
	
	bob = new Bob(250,250);	
	initRound();
	
	//assignation des commandes
	document.onkeydown = keyDown; 
	document.onkeyup = keyUp;
	canvas.onmousedown = mouseDown;
	canvas.onmouseup = mouseUp;
	
	tick();
});

function initRound(){	
	spriteList = new Array();	
	//ajout du premier sprite, le background
	spriteList.push(new BackgroundStars());
	spriteList.push(new BackgroundGrid());
	spriteList.push(bob);
	bulletList = new Array();
	
	setTimeout(function (){
				bob.isDead = false;
				}, 2000);
	
	//ajout enemies	
	setTimeout(spawnEnemies,6000);
	
}

function spawnEnemies(){
	spriteList.push(new BlueDiamond(Math.random() * 1000,Math.random() *740));
	
	spriteList.push(new PurpleTriangle(Math.random() * 1000,Math.random() *740));
	
	if(!bob.isDead)
		setTimeout(spawnEnemies, 1250);
}
	

function keyDown(e){
	var unicode = getEventCode(e);
	
	if(unicode == 87 || unicode == 38){
		upPressed = true;
	}
	
	else if(unicode == 83 || unicode == 40){
		downPressed = true;
	}
	
	else if(unicode == 65 || unicode == 37){
		leftPressed = true;
	}
	
	else if(unicode == 68 || unicode == 39){
		rightPressed = true;
	}
	
	else if(unicode == 32){	//spacebar
		//bob.bomb();
	}
}

function keyUp(e){
	var unicode = getEventCode(e);

	if(unicode == 87 || unicode == 38){
		upPressed = false;
	}
	
	else if(unicode == 83 || unicode == 40){
		downPressed = false;
	}
	
	else if(unicode == 65 || unicode == 37){
		leftPressed = false;
	}
	
	else if(unicode == 68 || unicode == 39){
		rightPressed = false;
	}
	
}

function mouseDown(e){
	//pour empêcher le default behavior
	e.preventDefault();
	e.stopPropagation();

	var unicode = getEventCode(e);	
	if(unicode == 1){
		mouseLeftPressed = true;
	}
}

function mouseUp(e){
	var unicode = getEventCode(e);	
	if(unicode == 1){
		mouseLeftPressed = false;
	}
}

function getMousePos(evt) {	// source: http://www.html5canvastutorials.com/advanced/html5-canvas-mouse-coordinates/
	var rect = canvas.getBoundingClientRect();
	return {
		x: evt.clientX - rect.left,
		y: evt.clientY - rect.top
	};
}

function tick(){

	//ctx.clearRect ( 0 , 0 , canvasWidth , canvasHeight );
	if(!bob.isDead){
	
		for(var i =0; i< spriteList.length; i++){
			var deleteSprite = spriteList[i].tick();
		
			if(deleteSprite){
				//ajout des points si on delete un sprite autre que le joueur
				if(!(spriteList[i] instanceof Bob)){
					score += spriteList[i].value;
				}
				
				//retrait du tableau
				spriteList.splice(i,1);
				i--;
			}
		}
		
		for(var i =0; i< bulletList.length; i++){		
			var deleteSprite = bulletList[i].tick();
		
			if(deleteSprite){
				//retrait du tableau
				bulletList.splice(i,1);
				i--;
			}
		}
	}
	else{
		//si il reste encore des vies
		if(bob.life >=0){
			initRound();
		}
	}
	
	//affichage du score du joueur
	ctx.font = "bold 24px sans-serif";
	ctx.fillText("Score :" + score, 248, 43);
	ctx.fillText("Lifes :" + bob.life, 400, 43);
	
	
	if(bob.isDead){
		setTimeout(tick, 3000);
	}
	else	
		setTimeout(tick,16.6666667);	// ~60fps
}


function BackgroundStars() {
	this.imageLoaded = false;		//etat si l'image est chargée ou pas en mémoire
	this.image = new Image();		//creation de l'objet image (natif JS)
	
	var sprite = this;				//affectation a une variable temporaire, pour pouvoir l'utiliser dans la function annonime
	
	this.image.onload = function(){
		sprite.imageLoaded = true;    //on utilise sprite au lieu de this car le this a l'interieur de la fontion n'est plus le this du backgroundsprite
	}
	this.image.src = "images/stars.png";  //source de l'image
	
	
	
	this.tick = function() {	
		if(this.imageLoaded){				//affichage seulement si l'image est loadé
			ctx.drawImage(this.image,0,0);
			
		}
	}
	
}

function BackgroundGrid() {
	this.imageLoaded = false;		//etat si l'image est chargée ou pas en mémoire
	this.image = new Image();		//creation de l'objet image (natif JS)
	
	var sprite = this;				//affectation a une variable temporaire, pour pouvoir l'utiliser dans la function annonime
	
	this.image.onload = function(){
		sprite.imageLoaded = true;    //on utilise sprite au lieu de this car le this a l'interieur de la fontion n'est plus le this du backgroundsprite
	}
	this.image.src = "images/grid.png";  //source de l'image
	
	
	
	this.tick = function() {
		if(this.imageLoaded){				//affichage seulement si l'image est loadé
			ctx.drawImage(this.image,0,0);
			
		}
	}
	
}


function Bob(x,y){
	this.x = x;
	this.y = y;
	this.angle = 0;
	this.size = 40;
	this.fireSpeed = 10; //frames
	this.fireCounter = this.fireSpeed;
	
	this.velocityX = 0;
	this.velocityY = 0;
	this.velocityFactor = 1.5;
	this.maxVelocity = 4;
	this.isDead = false;
	this.life = 3;
	
	this.shoot = function(){
		
		bulletList.push(new Bullet(this.x, this.y, convertToRadians(this.angle) - convertToRadians(90)));
	}
	
	this.tick = function(){
		//detection si touché
		for(var i = 0; i < spriteList.length; i++){
			if(!(spriteList[i] instanceof Bob)){
				if((spriteList[i].x > this.x - this.size/2) && (spriteList[i].x < this.x + this.size/2)){
					if((spriteList[i].y > this.y - this.size/2) && (spriteList[i].y < this.y + this.size/2)){
						this.isDead = true;
					}
				}
			}
		}
		if(!this.isDead){	
		
			if(this.velocityY < this.maxVelocity && this.velocityY > -this.maxVelocity){
				if(upPressed){
					this.velocityY -= this.velocityFactor;
				}
				if(downPressed){
					this.velocityY += this.velocityFactor;
				}	
			}	
			
			if(this.velocityX < this.maxVelocity && this.velocityX > -this.maxVelocity){
				if(leftPressed){
					this.velocityX -= this.velocityFactor;
				}
				if(rightPressed){
					this.velocityX += this.velocityFactor;
				}
			}
			//réajustement si trop grand
			if(this.velocityY > this.maxVelocity)
				this.velocityY = this.maxVelocity;
			else if(this.velocityY < -this.maxVelocity)
				this.velocityY = (-this.maxVelocity);
				
			if(this.velocityX > this.maxVelocity)
				this.velocityX = this.maxVelocity;
			else if(this.velocityX < -this.maxVelocity)
				this.velocityX = (-this.maxVelocity);
			
			if(!upPressed && !downPressed){
				this.velocityY = 0;
			}
			if(!leftPressed && !rightPressed){
				this.velocityX = 0;
			}
			
			
			//déplacement
			this.x += this.velocityX;
			this.y += this.velocityY;
			
			//collision avec le contour du canvas
			if(this.x > canvasWidth - this.size/2)
				this.x = canvasWidth - this.size/2;
			else if(this.x < this.size/2)
				this.x = this.size/2;
			if(this.y > canvasHeight - this.size/2)
				this.y = canvasHeight - this.size/2;
			else if(this.y < this.size/2)
				this.y = this.size/2;	
			
			//angle
			this.angle = getElementAngle(this.x,this.y,mousePos.x,mousePos.y);
			
			//doit il tirer
			if(mouseLeftPressed){
				if(this.fireCounter >= this.fireSpeed){
					this.fireCounter = 0;
					this.shoot();
				}			
			}
			
			this.fireCounter ++
			
			
			//se dessiner
			ctx.beginPath();
			ctx.arc(this.x,this.y,this.size/2,0, Math.PI*2,true); //dessiner le rond
			ctx.closePath();
			ctx.fillStyle = "rgba(0,255,0,0.85)";
			ctx.fill();
			
			ctx.beginPath();
			ctx.arc(this.x,this.y,(this.size/2) + 4,(convertToRadians(this.angle - 45 ) - convertToRadians(90)), (convertToRadians(this.angle + 45 ) - convertToRadians(90)),false); //dessiner le rond
			ctx.lineWidth = 5;
			ctx.strokeStyle = "rgba(250,157,7,1)";
			ctx.stroke();	
		}
		else
			this.life --;
		
		return this.isDead;
	}	
}

function Bullet(x,y,angle){
	this.x = x;
	this.y = y;
	this.angle = angle;
	
	this.width = 4;
	this.lenght = 40;
	
	this.speed = 8;
	
	this.tick = function(){
		
		var outOfCanvas = false;
		
		//update sa position
		var ap = getAngledPoint(this.angle,this.speed,this.x,this.y);
		this.x = ap.x;
		this.y = ap.y;
		
		//si hors du canvas
		if(this.x > canvasWidth || this.x < 0 || this.y > canvasHeight ||this.y < 0){
			outOfCanvas = true;
		}
		else{
			//se dessiner
			ap = getAngledPoint(this.angle,this.lenght,this.x,this.y);
			
			var grad = ctx.createLinearGradient(this.x,this.y, ap.x, ap.y);
			grad.addColorStop(1, '#FFF');
			grad.addColorStop(0.95, '#FFFB26');
			grad.addColorStop(0, 'rgba(0,0,0,0)');
			
			ctx.beginPath();
			ctx.moveTo(this.x, this.y);
			ctx.lineTo(ap.x, ap.y);
			ctx.strokeStyle = grad;
			ctx.lineWidth = this.width;
			ctx.stroke();
			
			
		}
		return outOfCanvas;
		
	}
	
	
}

function BlueDiamond(x,y){
	this.x = x;
	this.y = y;
	this.size = 25;	
	this.velocityX = 0;
	this.velocityY = 0;
	this.velocityFactor = 0.05;
	this.maxVelocity = 1.5;
	this.extensibility = 0;
	this.maxExtensibility = 5;
	this.down = true;
	this.value = 50;
		
	this.tick = function(){
		var isDead = false;
		
			
		//detection si touché
		for(var i = 0; i < bulletList.length; i++){
			if((bulletList[i].x > this.x - this.size/2) && (bulletList[i].x < this.x + this.size/2)){
				if((bulletList[i].y > this.y - this.size/2) && (bulletList[i].y < this.y + this.size/2)){
					isDead = true;
				}
			}
		}
		if(!isDead){		
		
			
			//pour le Y
			if(this.y < bob.y){
				if(this.velocityY < this.maxVelocity){
					this.velocityY += this.velocityFactor;
				}
				
			}
			else if(this.y > bob.y){
				if(this.velocityY > -this.maxVelocity){
					this.velocityY -= this.velocityFactor;
				}
				
			}
			
			//pour le X
			if(this.x < bob.x){
				if(this.velocityX < this.maxVelocity){
					this.velocityX += this.velocityFactor;
				}
				
			}
			else if(this.x > bob.x){
				if(this.velocityX > -this.maxVelocity){
					this.velocityX -= this.velocityFactor;
				}
				
			}
					
			this.x += this.velocityX;
			this.y += this.velocityY;
			
			//variability de la forme selon sa velocité
			if(this.down){
				this.extensibility -= (Math.max(Math.abs(this.velocityX),Math.abs(this.velocityY)) * 0.2);
				
				if(this.extensibility < -this.maxExtensibility)
					this.down = !this.down;
			}
			else{
				this.extensibility += (Math.max(Math.abs(this.velocityX),Math.abs(this.velocityY)) *0.2);
				
				if(this.extensibility > this.maxExtensibility)
					this.down = !this.down;
			}
			
			//collision avec le contour du canvas
			if(this.x > canvasWidth - this.size/2)
				this.x = canvasWidth - this.size/2;
			else if(this.x < this.size/2)
				this.x = this.size/2;
			if(this.y > canvasHeight - this.size/2)
				this.y = canvasHeight - this.size/2;
			else if(this.y < this.size/2)
				this.y = this.size/2;	
			
			
			//se dessiner
			ctx.beginPath();
			ctx.moveTo(this.x - this.size/2 - this.extensibility,this.y);
			ctx.lineTo(this.x, this.y -this.size/2 + this.extensibility);
			ctx.lineTo(this.x + this.size/2 + this.extensibility, this.y);
			ctx.lineTo(this.x, this.y + this.size/2 - this.extensibility);
			ctx.closePath();
			ctx.lineWidth = 3;
			ctx.strokeStyle = "#19CDFF";
			ctx.stroke();
		}
			
		return isDead;	
		
	}	
	
}
	
function PurpleTriangle(x,y){
	this.x = x;
	this.y = y;
	this.size = 25;	
	this.velocityX = 0;
	this.velocityY = 0;
	this.velocityFactor = 0.01;
	this.maxVelocity = 1;
	this.rotationAngle = 0;
	this.value = 25;
	
		
	this.tick = function(){
		var isDead = false;
		
			
		//detection si touché
		for(var i = 0; i < bulletList.length; i++){
			if((bulletList[i].x > this.x - this.size/2) && (bulletList[i].x < this.x + this.size/2)){
				if((bulletList[i].y > this.y - this.size/2) && (bulletList[i].y < this.y + this.size/2)){
					isDead = true;
				}
			}
		}
		if(!isDead){
				
			//pour le Y
			if(this.y < bob.y){
				if(this.velocityY < this.maxVelocity){
					this.velocityY += this.velocityFactor;
				}
				
			}
			else if(this.y > bob.y){
				if(this.velocityY > -this.maxVelocity){
					this.velocityY -= this.velocityFactor;
				}
				
			}
			
			//pour le X
			if(this.x < bob.x){
				if(this.velocityX < this.maxVelocity){
					this.velocityX += this.velocityFactor;
				}
				
			}
			else if(this.x > bob.x){
				if(this.velocityX > -this.maxVelocity){
					this.velocityX -= this.velocityFactor;
				}
				
			}
							
			this.x += this.velocityX;
			this.y += this.velocityY;
			
			//variability de la forme selon sa velocité
		
			this.rotationAngle -= (Math.max(Math.abs(this.velocityX),Math.abs(this.velocityY)) * 4);
			if(this.rotationAngle >= 120)
				this.rotationAngle = 0;
			
			
			
			//collision avec le contour du canvas
			if(this.x > canvasWidth - this.size/2)
				this.x = canvasWidth - this.size/2;
			else if(this.x < this.size/2)
				this.x = this.size/2;
			if(this.y > canvasHeight - this.size/2)
				this.y = canvasHeight - this.size/2;
			else if(this.y < this.size/2)
				this.y = this.size/2;	
			
			
			p2Degree = this.rotationAngle + 120;
			p3Degree = p2Degree + 120;
			
			p1 = getAngledPoint(convertToRadians(this.rotationAngle),this.size/2,this.x,this.y);
			p2 = getAngledPoint(convertToRadians(p2Degree),this.size/2,this.x,this.y);
			p3 = getAngledPoint(convertToRadians(p3Degree),this.size/2,this.x,this.y);
			
			//se dessiner
			ctx.beginPath();
			ctx.moveTo(p1.x,p1.y);
			ctx.lineTo(p2.x,p2.y);
			ctx.lineTo(p3.x,p3.y);
			ctx.closePath();
			ctx.lineWidth = 3;
			ctx.strokeStyle = "#D24DFF";
			ctx.stroke();
		}
		
		return isDead;
		
	}	
	
}