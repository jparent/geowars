#Geometry Wars inspired Game prototype

Simple Javascript game prototype **heavily** inspired by [Bizzare Creations](http://en.wikipedia.org/wiki/Bizarre_Creations)' [Geometry Wars](http://en.wikipedia.org/wiki/Geometry_Wars). 
First developed as a chat feature for a web programming course, I then stripped everything except the game so it will be easier to work on it again and play it.

To play, clone or download the source folder and lunch the index.html.

##Controls
- W,A,S,D or arrow keys to move
- Left mouse button to shoot